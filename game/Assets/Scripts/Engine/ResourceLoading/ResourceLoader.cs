﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

using AnyObject = System.Object;

public class ResourceLoader : IResourceLoader
{
    public GameObject LoadPrefab (string name)
    {
        GameObject prefab = (GameObject)Resources.Load ("Prefabs/" + name, typeof (GameObject));
        if (!prefab)
        {
            this.DebugWarning("Prefab " + name + " could not be loaded");
        }
        return prefab;
    }

    public Sprite LoadSprite (string name, int index)
    {
        Sprite[] sprite = Resources.LoadAll<Sprite> ("Sprites/" + name);
        if (index >= sprite.Length)
        {
            this.DebugWarning("Sprite " + name + " could not be loaded");
        }
        return sprite[index];
    }

    public AsyncRequest<string> LoadTextFileAsync(AnyObject caller, string filename)
    {
        AsyncRequest<string> request = new AsyncRequest<string>(caller);
        Coroutine routine = this.StartGlobalCoroutine(LoadTextFileRoutine(filename, request));
        request.SetRoutine(routine);
        return request;
    }

    private IEnumerator LoadTextFileRoutine(string filename, AsyncRequest<string> request)
    {
        string text = null;
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            //if we're on the web, we have to wait until the web request returns the real file path
            UnityWebRequest webRequest = UnityWebRequest.Get(filename);
            yield return webRequest.SendWebRequest();
            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                this.DebugWarning(webRequest.error);
            }
            else
            {
                text = webRequest.downloadHandler.text;
            }
        }
        else
        {
            text = File.ReadAllText(filename);
        }
        request.SetLoadedObject(text);
        request.OnObjectLoaded();
    }
}
