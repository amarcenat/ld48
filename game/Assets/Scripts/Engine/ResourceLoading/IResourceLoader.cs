﻿using UnityEngine;
using AnyObject = System.Object;

public interface IResourceLoader
{
    GameObject LoadPrefab(string name);
    Sprite LoadSprite(string name, int index);
    AsyncRequest<string> LoadTextFileAsync(AnyObject caller, string filename);
}

public class ResourceLoaderProxy : UniqueProxy<IResourceLoader>
{ }