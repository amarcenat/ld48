﻿using System.Collections.Generic;
using UnityEngine.Assertions;

public class ListenerNotifier
{
    private string m_Tag;
    private List<Listener> m_Listeners;

    public ListenerNotifier(string tag)
    {
        m_Tag = tag;
        m_Listeners = new List<Listener>();
    }

    public void Notify(GameEvent e)
    {
        Assert.IsTrue(e.GetTag() == m_Tag, "GameEvent has tag " + e.GetTag() + " but notifier has tag " + m_Tag);
        foreach (Listener listener in m_Listeners)
        {
            if (listener.IsGameEventHandled(e))
            {
                ReflectionHelper.CallMethod("OnGameEvent", listener.GetObjectToNotify(), e);
            }
        }
    }

    public void AddListener(System.Object objectToNotify, params System.Type[] GameEventTypes)
    {
        m_Listeners.Add(new Listener(objectToNotify, GameEventTypes));
    }

    public void RemoveListener(System.Object objectToNotify)
    {
        int indexToRemove = m_Listeners.FindIndex(
            delegate (Listener listener)
            {
                return listener.GetObjectToNotify() == objectToNotify;
            }
        );
        m_Listeners.RemoveAt(indexToRemove);
    }
}