﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCoordinator : MonoBehaviour
{
    private PlayerController m_Player1;
    private PlayerController m_Player2;
    private int m_BegunCount = 0;
    private int m_FinishedCount = 0;
    private bool m_CanProcessInput = true;

    public void Awake()
    {
        this.RegisterAsListener("Player", typeof(PlayerInputGameEvent));
        this.RegisterAsListener("Game", typeof(LevelEvent));
        PlayerController[] players = GetComponentsInChildren<PlayerController>();
        m_Player1 = players[0];
        m_Player2 = players[1];
    }

    private void OnDestroy()
    {
        this.UnregisterAsListener("Player");
        this.UnregisterAsListener("Game");
    }

    public void OnGameEvent(LevelEvent levelEvent)
    {
        m_Player1.Move(0, -1);
        m_Player2.Move(0, -1);
    }

    public void OnGameEvent(PlayerInputGameEvent inputEvent)
    {
        if (UpdaterProxy.Get().IsPaused() || !m_CanProcessInput)
        {
            return;
        }
        string input = inputEvent.GetInput();
        EInputState state = inputEvent.GetInputState();
        if (state == EInputState.Held || state == EInputState.Down)
        {
            switch (input)
            {
                case "Right":
                    m_Player1.Move(1, 0);
                    m_Player2.Move(1, 0);
                    m_Player1.FlipSprite(false);
                    m_Player2.FlipSprite(false);
                    break;
                case "Left":
                    m_Player1.Move(-1, 0);
                    m_Player2.Move(-1, 0);
                    m_Player1.FlipSprite(true);
                    m_Player2.FlipSprite(true);
                    break;
                default:
                    break;
            }
        }
        if (state == EInputState.Down)
        {
            switch (input)
            {
                case "Action":
                    m_CanProcessInput = false;
                    m_Player1.Action();
                    m_Player2.Action();
                    m_CanProcessInput = true;
                    break;
                case "Retry":
                    new GameFlowEvent(EGameFlowAction.Retry).Push();
                    break;
                default:
                    break;
            }
        }
    }

    public void HasBegun()
    {
        m_BegunCount++;
        m_CanProcessInput = false;
    }

    public void HasFinished()
    {
        m_FinishedCount++;
        if(m_FinishedCount == m_BegunCount)
        {
            m_FinishedCount = 0;
            m_BegunCount = 0;
            m_CanProcessInput = true;
            TileManagerProxy.Get().EvaluateLevel();
            m_Player1.Move(0, -1);
            m_Player2.Move(0, -1);
        }
    }
}
