﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject m_GhostPrefab;
    [SerializeField] private AudioClip m_DeathSound;
    private PlayerCoordinator m_Coordinator;
    private MovingTileObject m_MovingTileObject;
    private SpriteRenderer m_Sprite;
    private TileObject m_HeldObject;
    private Animator m_Animator;
    private EDirection m_FacingDirection = EDirection.Right;
    private List<SpriteRenderer> m_ArrowSprites = new List<SpriteRenderer>();
    private bool m_IsDead = false;

    void Awake()
    {
        m_Coordinator = GetComponentInParent<PlayerCoordinator>();
        m_MovingTileObject = GetComponent<MovingTileObject>();
        m_Sprite = GetComponentInChildren<SpriteRenderer>();
        m_Animator = GetComponentInChildren<Animator>();
    }

    public void FlipSprite(bool value)
    {
        if (value != m_Sprite.flipX)
        {
            m_Sprite.flipX = value;
            m_FacingDirection = value ? EDirection.Left : EDirection.Right;
            foreach (SpriteRenderer sr in m_ArrowSprites)
            {
                sr.flipX = value;
                Vector3 tmp = sr.transform.parent.localPosition;
                sr.transform.parent.localPosition = new Vector3(-tmp.x, tmp.y, tmp.z);
            }
        }
    }

    public bool CanBeDamagedByArrow(EDirection arrowIncomingDirection)
    {
        return m_HeldObject == null || arrowIncomingDirection != m_FacingDirection;
    }

    public void Move(int xDir, int yDir)
    {
        StartCoroutine(MoveRoutine(xDir, yDir));
    }

    IEnumerator MoveRoutine(int xDir, int yDir)
    {
        if (m_MovingTileObject.TryMove(xDir, yDir))
        {
            m_Coordinator.HasBegun();
            if(xDir != 0)
            {
                m_Animator.SetBool("IsMoving", true);
            }
            while (m_MovingTileObject.IsMoving())
            {
                yield return null;
            }
            m_Animator.SetBool("IsMoving", false);
            m_Coordinator.HasFinished();
        }
    }

    public void Action()
    {
        TileCoordinates coordinate = m_MovingTileObject.GetCoordinates();
        if (m_HeldObject != null)
        {
            m_HeldObject.SetIsEnabled(true);
            m_HeldObject.SetCoordinates(coordinate);
            m_HeldObject.transform.SetParent(null, true);
            m_HeldObject.GetComponentInChildren<SpriteRenderer>().enabled = true;
            TileManagerProxy.Get().EvaluateLevel();
            m_HeldObject = null;
            m_Animator.SetBool("IsHolding", false);
        }
        else
        {
            List<TileObject> tileObjects = TileManagerProxy.Get().GetObjectsInTile(coordinate);
            if (tileObjects == null)
            {
                return;
            }

            foreach (TileObject t in tileObjects)
            {
                if (t.GetObjectType() == ETileObjectType.Crate)
                {
                    m_HeldObject = t;
                    break;
                }
            }
            if(m_HeldObject != null)
            {
                m_HeldObject.transform.SetParent(transform, true);
                m_HeldObject.GetComponentInChildren<SpriteRenderer>().enabled = false;
                m_HeldObject.SetIsEnabled(false);
                TileManagerProxy.Get().EvaluateLevel();
                m_Animator.SetBool("IsHolding", true);
            }
        }
    }

    public void OnDeath()
    {
        if (!m_IsDead)
        {
            m_IsDead = true;
            m_Animator.SetTrigger("Death");
            GameObject ghost = Instantiate(m_GhostPrefab);
            ghost.transform.position = transform.position;
            SoundManagerProxy.Get().PlayMultiple(m_DeathSound);
        }
    }

    public void OnArrowHit(SpriteRenderer arrowSprite)
    {
        m_ArrowSprites.Add(arrowSprite);
        if (m_FacingDirection == EDirection.Left)
        {
            Vector3 tmp = arrowSprite.transform.parent.localPosition;
            arrowSprite.transform.parent.localPosition = new Vector3(-tmp.x, tmp.y, tmp.z);
        }
    }

    public void OnArrowDestroyed(SpriteRenderer arrowSprite)
    {
        m_ArrowSprites.Remove(arrowSprite);
    }
}
