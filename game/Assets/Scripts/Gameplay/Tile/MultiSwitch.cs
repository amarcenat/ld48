﻿using System.Collections.Generic;
using UnityEngine;

public class MultiSwitch : Tile
{
    [SerializeField] private List<GameObject> m_Objects;
    private Animator m_Animator;
    private bool m_IsActive = false;

    public override bool IsObstacle() { return false; }

    private string GetEventTag(GameObject g) { return g.GetInstanceID().ToString(); }

    new void Awake()
    {
        base.Awake();
        m_Animator = GetComponentInChildren<Animator>();
    }

    public override bool EvaluateRule()
    {
        bool newIsActive = false;
        List<TileObject> tileObjects = GetTileObjects();
        if (tileObjects != null)
        {
            foreach (TileObject t in tileObjects)
            {
                if (t.CanActivateSwitch())
                {
                    newIsActive = true;
                    break;
                }
            }
        }
        if (m_IsActive != newIsActive)
        {
            SetIsActive(newIsActive);
            return true;
        }
        return false;
    }

    public override void DoRule()
    {
        if (m_Objects == null)
        {
            return;
        }

        if (m_IsActive)
        {
            foreach (GameObject g in m_Objects)
            {
                new OnSwitchEnabled(GetEventTag(g)).Push();
            }
        }
        else
        {
            foreach (GameObject g in m_Objects)
            {
                new OnSwitchDisabled(GetEventTag(g)).Push();
            }
        }
    }

    private void SetIsActive(bool value)
    {
        m_IsActive = value;
        m_Animator.SetBool("IsOn", m_IsActive);
    }

    public bool IsActive()
    {
        return m_IsActive;
    }
}
