﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] private float m_MoveSpeed = 15f;
    [SerializeField] private bool m_ShouldBlockMovement = true;
    private bool m_IsMoving = false;
    private int m_Direction = 1;

    public void Move(int direction)
    {
        m_Direction = direction;
        SetIsMoving(true);
    }

    protected void SetIsMoving(bool isMoving)
    {
        if (m_IsMoving != isMoving)
        {
            m_IsMoving = isMoving;
            if (m_ShouldBlockMovement)
            {
                if (m_IsMoving)
                {
                    FindObjectOfType<PlayerCoordinator>().HasBegun();
                }
                else
                {
                    FindObjectOfType<PlayerCoordinator>().HasFinished();
                }
        }
        }
    }

    private TileCoordinates GetCoordinates()
    {
        return transform.position;
    }

    void Update()
    {
        if (m_IsMoving)
        {
            transform.position = new Vector3(transform.position.x + Time.deltaTime * m_MoveSpeed * m_Direction, transform.position.y, transform.position.z);

            TileCoordinates targetCoordinate = GetCoordinates(); // + new TileCoordinates(m_Direction, 0);
            Tile targetTile = TileManagerProxy.Get().GetTile(targetCoordinate);
            if (targetTile != null && targetTile.IsObstacle())
            {
                StartCoroutine(DeathRoutine(null));
            }
            List<TileObject> tileObjects = TileManagerProxy.Get().GetObjectsInTile(targetCoordinate);
            if (tileObjects != null)
            {
                foreach (TileObject t in tileObjects)
                {
                    if (t.GetObjectType() == ETileObjectType.Player)
                    {
                        PlayerController playerController = t.GetComponent<PlayerController>();
                        if (playerController.CanBeDamagedByArrow(m_Direction == -1 ? EDirection.Right : EDirection.Left))
                        {
                            playerController.OnDeath();
                            new GameFlowEvent(EGameFlowAction.GameOver).Push();
                        }
                        else
                        {
                            StartCoroutine(DeathRoutine(playerController));
                            transform.SetParent(t.transform);
                            transform.localPosition = new Vector3(0.77f, 0f, 0f);
                            playerController.OnArrowHit(GetComponentInChildren<SpriteRenderer>());
                        }
                        break;
                    }
                }
            }
        }
    }

    IEnumerator DeathRoutine(PlayerController playerController)
    {
        SetIsMoving(false);
        GetComponentInChildren<Animator>().SetTrigger("Hit");
        yield return new WaitForSeconds(2f);
        if (playerController != null)
        {
            playerController.OnArrowDestroyed(GetComponentInChildren<SpriteRenderer>());
        }
        Destroy(gameObject);
    }
}
