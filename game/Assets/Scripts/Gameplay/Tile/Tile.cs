﻿using System.Collections.Generic;
using UnityEngine;

public enum ETileType
{
    Invalid,
    Normal,
    Goal,
    Wall
}

public enum EDirection
{
    Right,
    Up,
    Left,
    Down
}

public class Tile : MonoBehaviour
{
    [SerializeField] private ETileType m_Type;
    private TileCoordinates m_Coordinates;
    private bool m_ShoudlDoRule = false;

    public void Awake()
    {
        m_Coordinates = transform.position;
        this.RegisterAsListener("Game", typeof(EvaluateRuleEvent), typeof(DoRuleEvent), typeof(LevelEvent));
    }

    public void OnDestroy()
    {
        this.UnregisterAsListener("Game");
    }

    public void OnGameEvent(LevelEvent levelEvent)
    {
        if (levelEvent.IsEntered())
        {
            TileManagerProxy.Get().AddTile(this);
        }
    }

    public void OnGameEvent(EvaluateRuleEvent evaluateRuleEvent)
    {
        m_ShoudlDoRule = EvaluateRule();
    }

    public void OnGameEvent(DoRuleEvent doRuleEvent)
    {
        if (m_ShoudlDoRule)
        {
            DoRule();
        }
        m_ShoudlDoRule = false;
    }

    public virtual bool IsObstacle() { return false; }

    public TileCoordinates GetCoordinates ()
    {
        return m_Coordinates;
    }

    public void SetCoordinates (TileCoordinates coordinates)
    {
        m_Coordinates = coordinates;
    }

    ETileType GetTileType()
    {
        return m_Type;
    }

    public void SetTileType (ETileType type)
    {
        m_Type = type;
    }

    public List<TileObject> GetTileObjects()
    {
        return TileManagerProxy.Get().GetObjectsInTile(GetCoordinates());
    }

    public virtual bool EvaluateRule()
    {
        return false;
    }

    public virtual void DoRule()
    {
    }

    public static Dictionary<EDirection, Vector2Int> ms_DirectionTileVector = new Dictionary<EDirection, Vector2Int>()
    {
        { EDirection.Right, new Vector2Int(1, 0) },
        { EDirection.Left,  new Vector2Int(-1, 0) },
        { EDirection.Up,    new Vector2Int(0, 1) },
        { EDirection.Down,  new Vector2Int(0, -1) },
    };
}
