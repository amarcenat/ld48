﻿using System.Collections.Generic;
using UnityEngine;

public class Spikes : Tile
{
    public override bool IsObstacle() { return false; }

    public override bool EvaluateRule()
    {
        List<TileObject> tileObjects = GetTileObjects();
        if(tileObjects == null)
        {
            return false;
        }

        foreach(TileObject t in tileObjects)
        {
            if(t.GetObjectType() == ETileObjectType.Player)
            {
                t.GetComponent<PlayerController>().OnDeath();
                return true;
            }
        }
        return false;
    }

    public override void DoRule()
    {
        new GameFlowEvent(EGameFlowAction.GameOver).Push();
    }
}
