﻿using System.Collections.Generic;
using UnityEngine;

public class Goal : Tile
{
    [SerializeField] private UnstableGroundFloor m_Floor;
    private Animator m_Animator;
    private bool m_IsActive = false;

    public override bool IsObstacle() { return false; }

    new void Awake()
    {
        base.Awake();
        m_Animator = m_Floor.GetComponentInChildren<Animator>();
    }

    public override bool EvaluateRule()
    {
        bool newIsActive = false;
        List<TileObject> tileObjects = GetTileObjects();
        if (tileObjects != null)
        {
            foreach (TileObject t in tileObjects)
            {
                if (t.GetObjectType() == ETileObjectType.Player)
                {
                    newIsActive = true;
                    break;
                }
            }
        }
        if (m_IsActive != newIsActive)
        {
            SetIsActive(newIsActive);
            return true;
        }
        return false;
    }

    public override void DoRule()
    {
    }

    private void SetIsActive(bool value)
    {
        m_IsActive = value;
        m_Animator.SetBool("IsOn", m_IsActive);
    }

    public bool IsActive()
    {
        return m_IsActive;
    }

    public void Open()
    {
        m_Floor.SetIsDestroyed(true);
        m_Animator.SetBool("IsOpened", true);
    }
}
