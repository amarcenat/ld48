﻿using System.Collections.Generic;
using UnityEngine;

public class OnSwitchEnabled : GameEvent
{
    public OnSwitchEnabled(string tag) : base(tag, EProtocol.Instant)
    {
    }
}

public class OnSwitchDisabled : GameEvent
{
    public OnSwitchDisabled(string tag) : base(tag, EProtocol.Instant)
    {
    }
}

public class Switch : Tile
{
    [SerializeField] private GameObject m_Object;
    private Animator m_Animator;
    private bool m_IsActive = false;

    public override bool IsObstacle() { return false; }

    private string GetEventTag() { return m_Object.GetInstanceID().ToString(); }

    new void Awake()
    {
        base.Awake();
        m_Animator = GetComponentInChildren<Animator>();
    }

    public override bool EvaluateRule()
    {
        bool newIsActive = false;
        List<TileObject> tileObjects = GetTileObjects();
        if (tileObjects != null)
        {
            foreach (TileObject t in tileObjects)
            {
                if (t.CanActivateSwitch())
                {
                    newIsActive = true;
                    break;
                }
            }
        }
        if (m_IsActive != newIsActive)
        {
            SetIsActive(newIsActive);
            return true;
        }
        return false;
    }

    public override void DoRule()
    {
        if (m_Object == null)
        {
            return;
        }

        if (m_IsActive)
        {
            new OnSwitchEnabled(GetEventTag()).Push();
        }
        else
        {
            new OnSwitchDisabled(GetEventTag()).Push();
        }
    }

    private void SetIsActive(bool value)
    {
        m_IsActive = value;
        m_Animator.SetBool("IsOn", m_IsActive);
    }

    public bool IsActive()
    {
        return m_IsActive;
    }
}
