﻿public class SwitchableTrap : Tile
{
    private string GetEventTag() { return gameObject.GetInstanceID().ToString(); }

    new public void Awake()
    {
        base.Awake();
        this.RegisterAsListener(GetEventTag(), typeof(OnSwitchEnabled), typeof(OnSwitchDisabled));
    }

    new public void OnDestroy()
    {
        this.UnregisterAsListener(GetEventTag());
        base.OnDestroy();
    }

    public void OnGameEvent(OnSwitchEnabled onEvent)
    {
        TrapOn();
    }

    public void OnGameEvent(OnSwitchDisabled onEvent)
    {
        TrapOff();
    }

    protected virtual void TrapOn() { }
    protected virtual void TrapOff() { }
}
