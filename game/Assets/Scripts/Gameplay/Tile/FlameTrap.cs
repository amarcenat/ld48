﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameTrap : MonoBehaviour
{
    public enum EState
    {
        Idle,
        Charging,
        Firing,
        WaitAnimation
    }

    [SerializeField] private UnstableGroundFloor m_Floor;
    [SerializeField] private float m_ChargingSpeed = 3f;
    [SerializeField] private float m_FireTime = 1f;
    private TileCoordinates m_Coordinates;
    private Animator m_FireAnimator;
    private Animator m_FloorAnimator;
    private EState m_State = EState.Idle;
    private float m_FireTimer = 0f;

    public void Awake()
    {
        m_Coordinates = transform.position;
        m_FireAnimator = GetComponentInChildren<Animator>();
        m_FloorAnimator = m_Floor.GetComponentInChildren<Animator>();
        StartCoroutine(StateMachineRoutine());
    }

    public void OnDestroy()
    {
    }

    IEnumerator StateMachineRoutine()
    {
        while (m_State != EState.WaitAnimation)
        {
            switch (m_State)
            {
                case EState.Idle:
                    m_FloorAnimator.SetBool("IsOn", false);
                    yield return null;
                    break;
                case EState.Charging:
                    m_FloorAnimator.SetBool("IsOn", true);
                    yield return new WaitForSeconds(m_ChargingSpeed);
                    m_State = EState.Firing;
                    break;
                case EState.Firing:
                    m_FireTimer = 0f;
                    m_FireAnimator.SetBool("Fire", true);
                    while (m_FireTimer < 0.5f)
                    {
                        m_FireTimer += Time.deltaTime;
                        if (Fire())
                        {
                            m_State = EState.WaitAnimation;
                        }
                        yield return null;
                    }
                    m_State = EState.Idle;
                    m_FireAnimator.SetBool("Fire", false);
                    break;
                default:
                    yield return null;
                    break;
            }
        }
    }

    public bool IsIdle()
    {
        return m_State == EState.Idle;
    }

    public void Activate()
    {
        if(IsIdle())
        {
            m_State = EState.Charging;
        }
    }

    private bool Fire()
    {
        List<TileObject> tileObjects = TileManagerProxy.Get().GetObjectsInTile(m_Coordinates);
        if (tileObjects == null)
        {
            return false;
        }

        foreach (TileObject t in tileObjects)
        {
            if (t.GetObjectType() == ETileObjectType.Player)
            {
                t.GetComponent<PlayerController>().OnDeath();
                new GameFlowEvent(EGameFlowAction.GameOver).Push();
                return true;
            }
        }
        return false;
    }

    public void Open()
    {
        m_Floor.SetIsDestroyed(true);
        m_FloorAnimator.SetBool("IsOn", true);
        m_FloorAnimator.SetBool("IsOpened", true);
        m_State = EState.WaitAnimation;
    }
}
