﻿using System.Collections.Generic;
using UnityEngine;

public class Door : SwitchableTrap
{
    [SerializeField] private AudioClip m_Sound;
    private Animator m_Animator;
    private bool m_IsClosed = true;

    new void Awake()
    {
        base.Awake();
        m_Animator = GetComponentInChildren<Animator>();
    }

    public override bool IsObstacle() { return m_IsClosed; }

    protected override void TrapOn()
    {
        SetIsClosed(false);
    }

    protected override void TrapOff()
    {
        SetIsClosed(true);
        List<TileObject> tileObjects = GetTileObjects();
        if (tileObjects != null)
        {
            foreach (TileObject t in tileObjects)
            {
                if (t.GetObjectType() == ETileObjectType.Player)
                {
                    t.GetComponent<PlayerController>().OnDeath();
                    new GameFlowEvent(EGameFlowAction.GameOver).Push();
                    break;
                }
            }
        }
    }

    private void SetIsClosed(bool value)
    {
        SoundManagerProxy.Get().PlayMultiple(m_Sound);
        m_IsClosed = value;
        m_Animator.SetBool("IsOpened", !m_IsClosed);
    }
}
