﻿using UnityEngine;

public class ArrowTrap : SwitchableTrap
{
    [SerializeField] private EDirection m_Direction = EDirection.Left;
    [SerializeField] private GameObject m_ArrowPrefab;
    [SerializeField] private Transform m_ArrowPosition;
    [SerializeField] private AudioClip m_Sound;
    private Animator m_Animator;

    public override bool IsObstacle() { return true; }

    new public void Awake()
    {
        base.Awake();
        m_Animator = GetComponent<Animator>();
    }

    protected override void TrapOn()
    {
        m_Animator.SetTrigger("Fire");
        GameObject arrow = Instantiate(m_ArrowPrefab);
        arrow.transform.position = m_ArrowPosition.position;
        arrow.GetComponent<Arrow>().Move(m_Direction == EDirection.Left ? -1 : 1);
        arrow.GetComponentInChildren<SpriteRenderer>().flipX = m_Direction == EDirection.Right;
        SoundManagerProxy.Get().PlayMultiple(m_Sound);
    }
}
