﻿using UnityEngine;

public class Wall : Tile
{
    public override bool IsObstacle() { return true; }
}
