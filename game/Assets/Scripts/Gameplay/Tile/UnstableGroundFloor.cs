﻿using UnityEngine;

public class UnstableGroundFloor : Tile
{
    private bool m_IsDestroyed = false;

    public override bool IsObstacle() { return !m_IsDestroyed; }

    public void SetIsDestroyed(bool value)
    {
        m_IsDestroyed = value;
        //GetComponentInChildren<SpriteRenderer>().enabled = false;
    }
}
