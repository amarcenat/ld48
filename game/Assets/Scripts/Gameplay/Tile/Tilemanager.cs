﻿using System.Collections.Generic;

public class TileManager
{
    private Dictionary<TileCoordinates, Tile> m_Tiles;
    private Dictionary<TileCoordinates, List<TileObject>> m_TileObjects;

    public TileManager ()
    {
        m_Tiles = new Dictionary<TileCoordinates, Tile>();
        m_TileObjects = new Dictionary<TileCoordinates, List<TileObject>> ();
    }

    public Tile GetTile (int x, int y)
    {
        return GetTile (new TileCoordinates (x, y));
    }

    public Tile GetTile (TileCoordinates coordinates)
    {
        Tile tile = null;
        m_Tiles.TryGetValue (coordinates, out tile);
        return tile;
    }

    public void AddTile (Tile tile)
    {
        m_Tiles.Add (tile.GetCoordinates (), tile);
    }

    public void AddTileObject(TileObject t)
    {
        List<TileObject> tileObjects = GetObjectsInTile(t.GetCoordinates());
        if(tileObjects == null)
        {
            tileObjects = new List<TileObject>();
            m_TileObjects.Add(t.GetCoordinates(), tileObjects);
        }
        tileObjects.Add(t);
    }

    public void RemoveTileObject(TileObject t)
    {
        List<TileObject> tileObjects = GetObjectsInTile(t.GetCoordinates());
        if (tileObjects != null)
        {
            tileObjects.Remove(t);
            if(tileObjects.Count == 0)
            {
                m_TileObjects.Remove(t.GetCoordinates());
            }
        }
    }

    public void UpdateTileObjects()
    {
        m_TileObjects.Clear();
        new UpdateTileObjectsEvent().Push();
    }

    public List<TileObject> GetObjectsInTile(TileCoordinates coordinates)
    {
        List<TileObject> tileObjects = null;
        m_TileObjects.TryGetValue(coordinates, out tileObjects);
        return tileObjects;
    }

    public void Reset ()
    {
        m_Tiles.Clear ();
        m_TileObjects.Clear();
    }

    public void EvaluateLevel()
    {
        UpdateTileObjects();
        new EvaluateRuleEvent().Push();
        new DoRuleEvent().Push();
        UpdateTileObjects();
    }
}

public class TileManagerProxy : UniqueProxy<TileManager>
{ }