﻿using System.Collections.Generic;
using UnityEngine;

public class UnstableGround : Tile
{
    [SerializeField] private int m_Health = 2;
    [SerializeField] private UnstableGroundFloor m_Floor;
    private Animator m_Animator;
    private int m_Count = 0;
    private int m_CurrentObjectCount = 0;

    public override bool IsObstacle() { return false; }

    new void Awake()
    {
        base.Awake();
        m_Animator = m_Floor.GetComponentInChildren<Animator>();
    }

    public override bool EvaluateRule()
    {
        List<TileObject> tileObjects = GetTileObjects();
        if (tileObjects == null)
        {
            m_CurrentObjectCount = 0;
            return false;
        }
        if (tileObjects.Count != m_CurrentObjectCount)
        {
            m_CurrentObjectCount = tileObjects.Count;
            return true;
        }
        return false;
    }

    public override void DoRule()
    {
        m_Count += m_CurrentObjectCount;
        m_Animator.SetInteger("Damage", m_Count);
        if (m_Count >= m_Health)
        {
            m_Floor.SetIsDestroyed(true);
        }
    }
}
