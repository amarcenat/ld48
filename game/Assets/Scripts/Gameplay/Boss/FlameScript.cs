﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameScript : MonoBehaviour
{
    protected List<FlameTrap> m_FlameTraps = new List<FlameTrap>();
    private bool m_IsPlaying = false;

    public void Init(List<FlameTrap> flameTraps)
    {
        m_FlameTraps = flameTraps;
    }

    public virtual void Play() 
    { 
        m_IsPlaying = true;
    }

    protected void Stop()
    {
        StartCoroutine(WaitForTraps());
    }

    public bool IsPlaying()
    {
        return m_IsPlaying;
    }

    IEnumerator WaitForTraps()
    {
        bool oneFlameIsNotIdle = true;
        while(oneFlameIsNotIdle)
        {
            yield return null;
            oneFlameIsNotIdle = false;
            foreach (FlameTrap f in m_FlameTraps)
            {
                if(!f.IsIdle())
                {
                    oneFlameIsNotIdle = true;
                    break;
                }
            }
        }
        m_IsPlaying = false;
    }
}
