﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeginFlameScript : GameEvent
{
    public BeginFlameScript() : base("Game", EProtocol.Instant)
    {
    }
}

public class OnFireAttackBegin : GameEvent
{
    public OnFireAttackBegin() : base("Game", EProtocol.Instant)
    {
    }
}

public class OnFireAttackEnd : GameEvent
{
    public OnFireAttackEnd() : base("Game", EProtocol.Instant)
    {
    }
}

public class FlameScriptManager : MonoBehaviour
{
    [SerializeField] private float m_TimeBetweenScripts = 5f;
    [SerializeField] private bool m_Loop = false;
    [SerializeField] private bool m_Randomize = false;
    private List<FlameScript> m_FlameScript = new List<FlameScript>();
    private List<FlameTrap> m_FlameTraps = new List<FlameTrap>();
    private int m_CurrentScriptIndex;

    public void Awake()
    {
        foreach (FlameTrap f in FindObjectsOfType<FlameTrap>())
        {
            m_FlameTraps.Add(f);
        }
        m_FlameTraps.Sort((f1, f2) => f1.transform.position.x.CompareTo(f2.transform.position.x));

        foreach (FlameScript fs in GetComponentsInChildren<FlameScript>())
        {
            fs.Init(m_FlameTraps);
            m_FlameScript.Add(fs);
        }
        this.RegisterAsListener("Game", typeof(BeginFlameScript));
    }

    private void OnDestroy()
    {
        this.UnregisterAsListener("Game");
    }

    public void OnGameEvent(BeginFlameScript flowEvent)
    {
        m_CurrentScriptIndex = 0;
        StopAllCoroutines();
        StartCoroutine(ScriptRoutine());
    }

    IEnumerator ScriptRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(m_TimeBetweenScripts);
            FlameScript fs = m_FlameScript[m_CurrentScriptIndex];
            fs.Play();
            new OnFireAttackBegin().Push();
            while (fs.IsPlaying())
            {
                yield return null;
            }
            new OnFireAttackEnd().Push();
            m_CurrentScriptIndex++;
            if (m_CurrentScriptIndex == m_FlameScript.Count)
            {
                if (m_Loop)
                {
                    m_CurrentScriptIndex = 0;
                }
                else
                {
                    break;
                }
            }
        }

        yield return new WaitForSeconds(2f);
        foreach (FlameTrap f in m_FlameTraps)
        {
            f.Open();
        }
        new GameFlowEvent(EGameFlowAction.EndLevel).Push();
        yield return new WaitForSeconds(2f);
        new GameFlowEvent(EGameFlowAction.NextAnimation).Push();
    }
}
