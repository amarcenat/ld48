﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimation : MonoBehaviour
{
    [SerializeField] private float m_XAmplitude = 0.5f;
    [SerializeField] private float m_YAmplitude = 0.5f;
    [SerializeField] private float m_XFrequency = 1f;
    [SerializeField] private float m_YFrequency = 1f;

    Vector3 m_PosOffset = new Vector3();
    Vector3 m_TempPos = new Vector3();

    [SerializeField] private Animator m_MouthAnimator;
    [SerializeField] private List<SpriteRenderer> m_Sprites;
    private int m_EyeDeadCount = 0;

    public void Awake()
    {
        m_PosOffset = transform.position;
        this.RegisterAsListener("Game", typeof(OnFireAttackBegin), typeof(OnFireAttackEnd), typeof(OnBossHit), typeof(OnBossEyeDead));
    }

    public void OnDestroy()
    {
        this.UnregisterAsListener("Game");   
    }

    public void OnGameEvent(OnFireAttackBegin fireAttackBegin)
    {
        m_MouthAnimator.SetBool("IsAngry", true);
    }

    public void OnGameEvent(OnFireAttackEnd fireAttackEnd)
    {
        m_MouthAnimator.SetBool("IsAngry", false);
    }

    public void OnGameEvent(OnBossHit onHit)
    {
        StartCoroutine(HitRoutine());
    }

    public void OnGameEvent(OnBossEyeDead onEyeDead)
    {
        m_EyeDeadCount++;
        if(m_EyeDeadCount >= 2)
        {
            StartCoroutine(BossDefeated());
        }
    }

    void Update()
    {
        m_TempPos = m_PosOffset;
        m_TempPos.x += Mathf.Sin(Time.fixedTime * Mathf.PI * m_XFrequency) * m_XAmplitude;
        m_TempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * m_YFrequency) * m_YAmplitude;

        transform.position = m_TempPos;
    }

    IEnumerator HitRoutine()
    {
        for (int i = 0; i < 10; i++)
        {
            foreach (SpriteRenderer sprite in m_Sprites)
            {
                sprite.color = Color.red;
            }
            yield return new WaitForSeconds(0.05f);
            foreach (SpriteRenderer sprite in m_Sprites)
            {
                sprite.color = Color.white;
            }
            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator BossDefeated()
    {
        foreach (FlameTrap f in FindObjectsOfType<FlameTrap>())
        {
            f.Open();
        }
        new GameFlowEvent(EGameFlowAction.EndLevel).Push();
        yield return new WaitForSeconds(2f);
        new GameFlowEvent(EGameFlowAction.NextAnimation).Push();
    }
}
