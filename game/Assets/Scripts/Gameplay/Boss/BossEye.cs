﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnBossHit : GameEvent
{
    public OnBossHit() : base("Game", EProtocol.Instant)
    {
    }
}

public class OnBossEyeDead : GameEvent
{
    public OnBossEyeDead() : base("Game", EProtocol.Instant)
    {
    }
}

public class BossEye : MonoBehaviour
{
    [SerializeField] private AudioClip m_Sound;
    [SerializeField] private Animator m_PupilleAnimator;
    [SerializeField] private int m_HP = 3;
    private bool m_IsHit = false;

    void OnTriggerEnter2D(Collider2D col)
    {
        col.GetComponent<BossArrow>().OnHit();
        if (!m_IsHit)
        {
            m_HP--;
            SetIsHit(true);
            new OnBossHit().Push();
            SoundManagerProxy.Get().PlayMultiple(m_Sound);
            if (m_HP == 0)
            {
                new OnBossEyeDead().Push();
            }
            else
            {
                StartCoroutine(HitRoutine());
            }
        }
    }

    IEnumerator HitRoutine()
    {
        yield return new WaitForSeconds(3f);
        SetIsHit(false);
    }

    private void SetIsHit(bool value)
    {
        m_IsHit = value;
        GetComponent<Animator>().SetBool("IsHit", m_IsHit);
        m_PupilleAnimator.SetTrigger("IsHit");
    }
}
