﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameScript_Barrage : FlameScript
{
    [SerializeField] private int m_FlameStartIndex;
    [SerializeField] private int m_FlameEndIndex;
    [SerializeField] private float m_TimeBetweenFlames = 0.5f;
    [SerializeField] private bool m_Reverse = false;
    private int m_CurrentIndex;

    public override void Play()
    {
        base.Play();
        m_CurrentIndex = m_Reverse ? m_FlameEndIndex : m_FlameStartIndex;
        StartCoroutine(ScriptRoutine());
    }

    IEnumerator ScriptRoutine()
    {
        while ((!m_Reverse && m_CurrentIndex <= m_FlameEndIndex) 
            || (m_Reverse && m_CurrentIndex >= m_FlameStartIndex))
        {
            m_FlameTraps[m_CurrentIndex].Activate();
            yield return new WaitForSeconds(m_TimeBetweenFlames);
            m_CurrentIndex += m_Reverse ? -1 : 1;
        }
        Stop();
    }
}
