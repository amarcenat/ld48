﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossArrow : Arrow
{
    public void OnHit()
    {
        SetIsMoving(false);
        Destroy(gameObject);
    }
}
