﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameScript_SilmutaneousFlame : FlameScript
{
    [SerializeField] private List<int> m_FlameIndexes;

    public override void Play()
    {
        base.Play();
        foreach (int index in m_FlameIndexes)
        {
            m_FlameTraps[index].Activate();
        }
        Stop();
    }
}
