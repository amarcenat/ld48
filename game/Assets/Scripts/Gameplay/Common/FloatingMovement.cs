﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingMovement : MonoBehaviour
{
    [SerializeField] private float m_XAmplitude = 0.5f;
    [SerializeField] private float m_YAmplitude = 0.5f;
    [SerializeField] private float m_XFrequency = 1f;
    [SerializeField] private float m_YFrequency = 1f;

    Vector3 m_PosOffset = new Vector3();
    Vector3 m_TempPos = new Vector3();

    public void Awake()
    {
        m_PosOffset = transform.position;
    }

    void Update()
    {
        m_TempPos = m_PosOffset;
        m_TempPos.x += Mathf.Sin(Time.fixedTime * Mathf.PI * m_XFrequency) * m_XAmplitude;
        m_TempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * m_YFrequency) * m_YAmplitude;

        transform.position = m_TempPos;
    }
}
