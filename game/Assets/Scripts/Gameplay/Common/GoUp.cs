﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoUp : MonoBehaviour
{
    [SerializeField] float m_MoveSpeed = 1f;

    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + Time.deltaTime * m_MoveSpeed, transform.position.z);
    }
}
