﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTileObject : TileObject
{
    [SerializeField] private float m_MoveSpeed = 10f;
    [SerializeField] private float m_FallSpeed = 15f;

    private bool m_IsMoving = false;
    private float m_SpeedModifier;
    private Vector3 m_TargetPos;

    public bool TryMove(int xDir, int yDir)
    {
        if (EvaluateMoveTo(xDir, yDir))
        {
            Move(xDir, yDir);
            return true;
        }
        return false;
    }

    public void Move(int xDir, int yDir)
    {
        SetTargetPos(xDir, yDir);
        StartCoroutine(MoveRoutine());
    }

    public void MoveInstant(int xDir, int yDir)
    {
        StopMovement();
        SetTargetPos(xDir, yDir);
        transform.position = m_TargetPos;
        SnapOnCurrentCoordinates();
    }

    public bool IsMoving()
    {
        return m_IsMoving;
    }

    private void SetTargetPos(int xDir, int yDir)
    {
        TileCoordinates targetCoordinate = GetCoordinates() + new TileCoordinates(xDir, yDir);
        SetCoordinates(targetCoordinate);
        m_TargetPos = new Vector3(transform.position.x + xDir, transform.position.y + yDir, transform.position.z);
        m_SpeedModifier = yDir < 0 ? m_FallSpeed : m_MoveSpeed;
    }

    private void SetIsMoving(bool isMoving)
    {
        m_IsMoving = isMoving;
    }

    IEnumerator MoveRoutine()
    {
        SetIsMoving(true);
        while (transform.position != m_TargetPos)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_TargetPos, Time.deltaTime * m_SpeedModifier);
            yield return null;
        }
        SnapOnCurrentCoordinates();
        SetIsMoving(false);
    }

    private void SnapOnCurrentCoordinates()
    {
        TileCoordinates currentCoordinates = GetCoordinates();
        Vector3 currentPosition = new Vector3(currentCoordinates.x, currentCoordinates.y, 0);
        transform.position = currentPosition;
    }

    private void StopMovement()
    {
        if (IsMoving())
        {
            StopAllCoroutines();
            SetIsMoving(false);
            transform.position = m_TargetPos;
        }
    }

    private bool EvaluateMoveTo(int xDir, int yDir)
    {
        if (IsMoving())
        {
            return false;
        }
        TileCoordinates targetCoordinate = GetCoordinates() + new TileCoordinates(xDir, yDir);
        Tile targetTile = TileManagerProxy.Get().GetTile(targetCoordinate);
        if(targetTile != null && targetTile.IsObstacle())
        {
            return false;
        }

        List<TileObject> tileObjects = TileManagerProxy.Get().GetObjectsInTile(targetCoordinate);
        if (tileObjects != null)
        {
            foreach (TileObject t in tileObjects)
            {
                if (t.GetObjectType() == ETileObjectType.Player)
                {
                    return false;
                }
            }
        }

        return true;
    }
}
