﻿using UnityEngine;

public enum ETileObjectType
{
    None,
    Player,
    Crate
}

public class TileObject : MonoBehaviour
{
    [SerializeField] private ETileObjectType m_Type;

    private TileCoordinates m_Coordinates;
    private bool m_IsEnabled = true;

    public void Awake()
    {
        m_Coordinates = transform.position;
        this.RegisterAsListener("Game", typeof(UpdateTileObjectsEvent));
    }

    public void OnDestroy()
    {
        this.UnregisterAsListener("Game");
    }

    public void SetIsEnabled(bool value)
    {
        m_IsEnabled = value;
    }

    public void OnGameEvent(UpdateTileObjectsEvent updateTileObjectsEvent)
    {
        if (m_IsEnabled)
        {
            TileManagerProxy.Get().AddTileObject(this);
        }
    }

    public ETileObjectType GetObjectType()
    {
        return m_Type;
    }

    public void SetObjectType(ETileObjectType type)
    {
        m_Type = type;
    }

    public TileCoordinates GetCoordinates()
    {
        return m_Coordinates;
    }

    public void SetCoordinates(TileCoordinates coordinates)
    {
        m_Coordinates = coordinates;
    }

    public virtual bool CanActivateSwitch() { return false; }
}
