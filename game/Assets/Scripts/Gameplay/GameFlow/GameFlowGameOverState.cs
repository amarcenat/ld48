﻿public class GameFlowGameOverState : HSMState
{
    public override void OnEnter()
    {
        UpdaterProxy.Get().SetPause(true);
        new LevelEvent(false).Push();
        this.RegisterAsListener("Game", typeof(GameFlowEvent));
    }

    public void OnGameEvent(GameFlowEvent flowEvent)
    {
        switch (flowEvent.GetAction())
        {
            case EGameFlowAction.NextAnimation:
                ChangeNextTransition(HSMTransition.EType.Clear, typeof(GameFlowNormalState));
                break;
        }
    }

    public override void OnExit()
    {
        this.UnregisterAsListener("Game");
        UpdaterProxy.Get().SetPause(false);
    }
}
