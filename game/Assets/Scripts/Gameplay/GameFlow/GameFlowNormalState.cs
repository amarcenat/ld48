﻿
using UnityEngine.SceneManagement;

public class GameFlowNormalState : HSMState
{
    public override void OnEnter ()
    {
        LevelManagerProxy.Get ().LoadCurrentLevel();
        this.RegisterAsListener ("Game", typeof (GameFlowEvent));
        this.RegisterAsListener ("Player", typeof (PlayerInputGameEvent));
    }


    public void OnGameEvent (PlayerInputGameEvent inputEvent)
    {
        if (inputEvent.GetInput () == "Pause" && inputEvent.GetInputState() == EInputState.Down && !UpdaterProxy.Get().IsPaused())
        {
            ChangeNextTransition (HSMTransition.EType.Child, typeof (GameFlowPauseState));
        }
    }

    public void OnGameEvent (GameFlowEvent flowEvent)
    {
        switch (flowEvent.GetAction ())
        {
            case EGameFlowAction.StartDialogue:
                ChangeNextTransition (HSMTransition.EType.Child, typeof (GameFlowDialogueState));
                break;
            case EGameFlowAction.Retry:
                new LevelEvent(false).Push();
                LevelManagerProxy.Get().LoadCurrentLevel();
                break;
            case EGameFlowAction.GameOver:
                ChangeNextTransition(HSMTransition.EType.Clear, typeof(GameFlowGameOverState));
                break;
            case EGameFlowAction.EndLevel:
                ChangeNextTransition(HSMTransition.EType.Clear, typeof(GameFlowEndLevelState));
                break;
        }
    }

    public override void OnExit ()
    {
        this.UnregisterAsListener ("Player");
        this.UnregisterAsListener ("Game");
    }
}