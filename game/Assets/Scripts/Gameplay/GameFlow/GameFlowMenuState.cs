﻿using UnityEngine.SceneManagement;

public class GameFlowMenuState : HSMState
{
    public override void OnEnter ()
    {
        SceneManager.LoadScene (0);
        this.RegisterAsListener ("Game", typeof (GameFlowEvent));
    }

    public void OnGameEvent (GameFlowEvent flowEvent)
    {
        switch (flowEvent.GetAction())
        {
            case EGameFlowAction.Start:
                ChangeNextTransition(HSMTransition.EType.Clear, typeof(GameFlowNormalState));
                break;
        }
    }

    public override void OnExit ()
    {
        this.UnregisterAsListener ("Game");
    }
}