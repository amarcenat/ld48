﻿using System.Collections;
using UnityEngine;

public class EndConditionChecker : MonoBehaviour
{
    [SerializeField] private Goal m_Goal1;
    [SerializeField] private Goal m_Goal2;
    private bool m_IsConditionMet = false;

    void Update()
    {
        if(m_IsConditionMet)
        {
            return;
        }
        if(m_Goal1.IsActive() && m_Goal2.IsActive())
        {
            m_IsConditionMet = true;
            m_Goal1.Open();
            m_Goal2.Open();
            new GameFlowEvent(EGameFlowAction.EndLevel).Push();
            StartCoroutine(WaitForAnimation());
        }
    }

    IEnumerator WaitForAnimation()
    {
        yield return new WaitForSeconds(1f);
        new GameFlowEvent(EGameFlowAction.NextAnimation).Push();
    }
}
