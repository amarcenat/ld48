﻿public class GameFlowEndLevelState : HSMState
{
    private int m_SubState = 0;

    public override void OnEnter()
    {
        m_SubState = 0;
        UpdaterProxy.Get().SetPause(true);
        this.RegisterAsListener("Game", typeof(GameFlowEvent));
    }

    public void OnGameEvent(GameFlowEvent flowEvent)
    {
        switch (flowEvent.GetAction())
        {
            case EGameFlowAction.NextAnimation:
                if(m_SubState == 0)
                {
                    LevelManagerProxy.Get().NextLevel();
                    new LevelEvent(false).Push();
                    m_SubState = 1;
                }
                else if (m_SubState == 1)
                {
                    ChangeNextTransition(HSMTransition.EType.Clear, typeof(GameFlowNormalState));
                }
                break;
        }
    }

    public override void OnExit()
    {
        this.UnregisterAsListener("Game");
        UpdaterProxy.Get().SetPause(false);
    }
}
