﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public class CanvasScaleFactorAdjuster : MonoBehaviour
{
    public Camera m_MainCamera;

    void Start()
    {
        AdjustScalingFactor();
    }

    void LateUpdate()
    {
        AdjustScalingFactor();
    }

    void AdjustScalingFactor()
    {
        GetComponent<CanvasScaler>().scaleFactor = m_MainCamera.GetComponent<UnityEngine.U2D.PixelPerfectCamera>().pixelRatio;
    }

}
