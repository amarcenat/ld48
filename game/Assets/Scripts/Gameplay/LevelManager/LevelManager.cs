﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelEvent : GameEvent
{
    public LevelEvent(bool enter) : base("Game", EProtocol.Instant)
    {
        m_Enter = enter;
    }

    public bool IsEntered ()
    {
        return m_Enter;
    }

    private bool m_Enter;
}

public class LevelManager
{
    private int m_CurrentLevel = 0;
    private Dictionary<int, string> m_LevelIdToName;
    private AsyncRequest<string> m_LevelNamesAsyncLoadRequest = null;
    private List<string> m_HeardDialogues = new List<string>();

    private static string ms_LevelFilename = "/LevelNames.txt";

    public LevelManager()
    {
        m_LevelIdToName = new Dictionary<int, string>();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    ~LevelManager()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public void OnEngineStart()
    {
        // Hack to disable dialogue on some levels
        m_HeardDialogues.Clear();
        m_HeardDialogues.Add("Movement_1");
        m_HeardDialogues.Add("Arrow_1");
        m_HeardDialogues.Add("Tuto_protection");
        m_HeardDialogues.Add("Crate_and_Arrow");
        m_HeardDialogues.Add("Doors_1");
        m_HeardDialogues.Add("Tuto_UnstableGround");

        string filename = Application.streamingAssetsPath + ms_LevelFilename;
        m_LevelNamesAsyncLoadRequest = ResourceLoaderProxy.Get().LoadTextFileAsync(this, filename);
    }

    public void OnObjectLoaded(string text)
    {
        FillLevelNames(text);
        m_LevelNamesAsyncLoadRequest = null;
    }

    public void LoadCurrentLevel()
    {
        SceneManager.LoadScene(m_CurrentLevel+1); // +1 because 0 is the menu scene
    }

    private void OnSceneLoaded (Scene scene, LoadSceneMode mode)
    {
        int buildIndex = scene.buildIndex;
        if (buildIndex > 0)
        {
            TileManagerProxy.Get().Reset();
            new LevelEvent(true).Push();
            new UpdateTileObjectsEvent().Push();
            string dialogueName = SceneManager.GetActiveScene().name;
            if (!m_HeardDialogues.Contains(dialogueName))
            {
                m_HeardDialogues.Add(dialogueName);
                Dialogue.DialogueManagerProxy.Get().TriggerDialogue(dialogueName);
            }
            else
            {
                new BeginFlameScript().Push();
            }
        }
    }

    public void SetLevelIndex(int levelIndex)
    {
        m_CurrentLevel = levelIndex;
    }

    public bool IsLastLevel()
    {
        return m_CurrentLevel == m_LevelIdToName.Count - 1;
    }

    public void NextLevel()
    {
        if (!IsLastLevel())
        {
            m_CurrentLevel++;
        }
        else 
        {
            m_CurrentLevel = 0;
        }
    }

    public string GetCurrentLevelName()
    {
        return m_LevelIdToName[m_CurrentLevel];
    }

    public int GetCurrentLevelID()
    {
        return m_CurrentLevel;
    }

    public Dictionary<int, string> GetLevelNames()
    {
        return m_LevelIdToName;
    }

    private void FillLevelNames(string text)
    {
        char[] lineSeparators = { '\n' };
        char[] separators = { ':' };

        string[] lines = text.Split(lineSeparators);

        for (int i = 0; i < lines.Length; i++)
        {
            string[] datas = lines[i].Split(separators);

            // If there is an error in print a debug message
            if (datas.Length != 2)
            {
                this.DebugLog("Invalid number of data line " + i + " expecting 2, got " + datas.Length);
                return;
            }

            int levelIndex = Int32.Parse((String)datas.GetValue(0)); ;
            string levelName = datas[1];
            m_LevelIdToName.Add(levelIndex, levelName);
        }
    }
}

public class LevelManagerProxy : UniqueProxy<LevelManager>
{ }
