﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DepthLevel : MonoBehaviour
{
    [SerializeField] private Text m_Text;

    void Start()
    {
        m_Text.text = "-" + (LevelManagerProxy.Get().GetCurrentLevelID() + 1);
    }
}
